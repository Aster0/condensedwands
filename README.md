# CondensedWands

CondensedWands is a plugin that allows you to condense ore(s) that are located in chests. This plugin is made specially for Skyvele.com.


# Description

**How does it work?**

First, you give yourself a wand via /cw give <name> <uses>, leaving <uses> empty would give you an infinite wand.
Secondly, just right click a chest and see the magic happen!

![](https://i.gyazo.com/70e601ff8f6c2d78402b4dae5cbb2e54.mp4)


# Video (Click image)
[![A full showcase YouTube video can be found below.](https://i.gyazo.com/116a2b24ca95089c6e0404b4148c34d8.png)](https://www.youtube.com/watch?v=fe1Dq32npLg&feature=youtu.be "")


# Contact Me
https://www.astero.me/